function addTokens(input, tokens = []) {
    if (typeof input !== 'string') throw new Error('Invalid input');

    if (input.length < 6) throw new Error('Input should have at least 6 characters')

    if (tokens.some(({ tokenName }) => typeof tokenName !== 'string')) throw new Error('Invalid array format');


    const iterators = tokens.values();

    return input.split('...').reduce((accumulator, currentValue) => {
        const { value = {} } = iterators.next() || {};
        const { tokenName = "" } = value;

        if (tokenName) {
            return accumulator.concat('${', tokenName, '}', currentValue);
        }

        return accumulator.concat(currentValue);

    });
}

const app = {
    addTokens: addTokens
}

module.exports = app;